package me.alvarez.dark;

//import java.util.Set;

import java.util.Set;

import org.bukkit.plugin.java.JavaPlugin;

public class WelcomeReward extends JavaPlugin{
	
	WelcomeReward plugin;
	public String msg;
	public int sec;
	public int[] reward;
	public int[] quantity;

	@Override
	public void onEnable(){
		plugin = this;
		
		plugin.saveDefaultConfig();
		
		String version_location = "http://www.brandonalvarez.com/plugins/versions/WelcomeReward.txt";
		String plugin_location = "http://www.brandonalvarez.com/plugins/plugins/WelcomeReward.jar";
		Updater Updater = new Updater(plugin);
		Updater.update(version_location, plugin_location);
		
		getConfigInfo();
		
		plugin.getServer().getPluginManager().registerEvents(new WRListener(plugin), this);
		System.out.println("<<WelcomeReward>> v" + this.getDescription().getVersion() + " is now enabled!");
		System.out.println("<<WelcomeReward>> by Alvarez, inspired by Prorockband");
	}

	@Override
	public void onDisable(){
		System.out.println("<<WelcomeReward>> v" + this.getDescription().getVersion() + " is now disabled!");
	}
	

	public void getConfigInfo() {
		
		sec = plugin.getConfig().getInt("TimeOutinSeconds");
		msg = plugin.getConfig().getString("RewardMessage");
		Set<String> num = this.getConfig().getConfigurationSection("RewardsItems").getKeys(false);
		
		reward = new int[num.size()];
		quantity = new int[num.size()];
		for(int i=0; i<num.size(); i++){
			String[] s = plugin.getConfig().getString("RewardsItems." + (i+1)).split(",");
			reward[i] = Integer.parseInt(s[0]);
			quantity[i] = Integer.parseInt(s[1]);
		}
		System.out.println("<<WelcomeReward>> Rewards Stored");
		
	}
}
