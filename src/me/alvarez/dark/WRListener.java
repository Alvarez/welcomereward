package me.alvarez.dark;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class WRListener implements Listener {

	private WelcomeReward plugin;
	public boolean waiting = false;
	
	
	public WRListener(WelcomeReward i){
		this.plugin = i;
	}

	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent  e){
		if(!waiting) return;
		String mess = e.getMessage().toLowerCase();
		if(!mess.contains("welcome")) return;
		
		Player player = e.getPlayer();
		
		player.sendMessage(ChatColor.GOLD + "[" + ChatColor.DARK_BLUE + "WelcomeReward" + ChatColor.GOLD + "]" + ChatColor.WHITE + plugin.msg);
		for(int i=0;i<plugin.reward.length; i++){
			ItemStack stack = new ItemStack(plugin.reward[i], plugin.quantity[i]);
			player.getInventory().addItem(stack);
		}
		
		waiting = false;
		plugin.getServer().getScheduler().cancelTasks(plugin);
		
	}
	
	@EventHandler
	public void onJoin(PlayerLoginEvent e){
		if(!e.getPlayer().hasPlayedBefore()){
			waiting = true;
			
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new BukkitRunnable(){
			    public void run(){
			        waiting = false;
			    }
			}, plugin.sec * 20);	
		}
	}
	
}
